class Adder implements Command {
 //Command design pattern
Command_Reception action;
Factory factory = new Factory();
  @Override //DataContext
  public void setDataContext(Object o) {
        action = (Command_Reception)(o);
  }
  
   @Override // Command
    public void execute() {
      //*ADDING BOID
      if(action.team=='b')
      {
        action.flock.boids.add(factory.createBoid(width/4, height/2, 'b'));
      }
      else if (action.team =='r')
      {
        action.flock.boids.add(factory.createBoid((width/4)*3, height/2, 'r'));
      }
      else if(action.team=='n')
      {
        action.flock.boids.add(factory.createBoid((width/4)*3, height/2, 'n'));
      }
  }
}


//**CLASS DELETE
class Deleter implements Command {
Command_Reception action;

  @Override //DataContext
  public void setDataContext(Object o) {
        action = (Command_Reception)(o);
  }

  @Override // Command
  public void execute() {
    
    if(action.team=='b')
    {
        for (BoidGen b : action.flock.boids) {
          if(b.squad=='b')
          {
            action.flock.dead=b;
            action.flock.oneDown=true;
          }  
      }   
    }
    else if (action.team =='r')
    {
       for (BoidGen b : action.flock.boids) {
          if(b.squad=='r')
          {
            action.flock.dead=b;
            action.flock.oneDown=true;
          }  
      }
    }
    else if(action.team=='n')
    {
       for (BoidGen b : action.flock.boids) {
          if(b.squad=='n')
          {
            action.flock.dead=b;
            action.flock.oneDown=true;
          }  
      }
    }
    
    if(action.flock.oneDown)
    {
      action.flock.oneDown=false;
      action.flock.boids.remove(action.flock.dead);
    }
    
  }
}

//** AUTOSPAWN CHANGER
class Spawner implements Command {
Command_Reception action;

  @Override //DataContext
  public void setDataContext(Object o) {
        action = (Command_Reception)(o);
  }

  @Override // Command
  public void execute() 
  {
    if(action.flock.autoSpawn==true)
    action.flock.autoSpawn=false;
    else
    action.flock.autoSpawn=true;
  }
}
