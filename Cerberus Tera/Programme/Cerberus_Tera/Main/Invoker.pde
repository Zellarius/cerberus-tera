class Invoker
{ //ArrayList<Boid> boids;   new ArrayList<Boid>();
    //Commands invoker
    private final HashMap commandMap = new HashMap<String, Command>();    
    
    public void register(String commandName, Command command) {
        commandMap.put(commandName, command);
    }
    
    public void execute(String commandName) {
        Command command = (Command)(commandMap.get(commandName));
        if (command == null) {
            throw new IllegalStateException("no command registered for " + commandName);
        }
        command.execute();
    }   
}
 
