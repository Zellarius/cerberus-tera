class Galaxy {
      int nbBlue=20;
      int nbRed=10;
      int nbNeutral=20;
      Flock spaceBoids;  // Boids
      Tera Euphor;
      PApplet p; // Required for Singleton

  Galaxy(PApplet papp)
  {
      p=papp;
      Euphor = new Tera(p); //MapGenerator
      spaceBoids = new Flock(nbRed,nbBlue,nbNeutral); 
      Euphor.initialisation();
  }
  
  void RunState()
  {
      Euphor.display(); 
      spaceBoids.run(); 
  }
} 
