/** The Command interface */
//Command used for networking reception
interface Command {
    void execute();
    void setDataContext(Object o);
}
