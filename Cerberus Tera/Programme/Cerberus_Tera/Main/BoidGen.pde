public abstract class BoidGen
{
  //Generic Boid class for factory and simplifying the declaration of boids
  PVector position;
  PVector velocity;
  PVector acceleration;
  char squad;
  float r;
  float maxforce;    // Maximum steering force
  float maxspeed;    // Maximum speed
  int state=0; // 0=Flocking 1=Attacking
  float theta=0;
  
  int currenttime;
  int wait;
  int lasttime;
  boolean del;
  

//////////////////////// FUNCTIONS THAT GETS OVERRIDE
   void flock(ArrayList<BoidGen> boids) {}
   void render() {}
   PVector separate (ArrayList<BoidGen> boids) { return new PVector();}
   PVector align (ArrayList<BoidGen> boids){return new PVector();}
   PVector cohesion (ArrayList<BoidGen> boids){return new PVector();}

//////////////////////// RUN
    void run(ArrayList<BoidGen> boids) {
    flock(boids);
    update();
    borders();
    render();
  }

//////////////////////// APPLYFORCE
    void applyForce(PVector force) {
    // We could add mass here if we want A = F / M
    acceleration.add(force);
  }
  
//////////////////////// UPDATE
  void update() {
  // Update velocity
    velocity.add(acceleration);
  // Limit speed
    if(state!=1)
    {
      velocity.limit(maxspeed);
    }
    position.add(velocity);
    // Reset accelertion to 0 each cycle
    acceleration.mult(0);
  }
  
//////////////////////// SEEK  
   PVector seek(PVector target) {
    PVector desired = PVector.sub(target, position);  // A vector pointing from the position to the target
    // Scale to maximum speed
    desired.normalize();
    desired.mult(maxspeed);

    // Steering = Desired minus Velocity
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxforce);  // Limit to maximum steering force
    return steer;
  }
  


//////////////////////// BORDERS (WRAPAROUND)  
  void borders() {
    if (position.x < -r) position.x = width+r;
    if (position.y < -r) position.y = height+r;
    if (position.x > width+r) position.x = -r;
    if (position.y > height+r) position.y = -r;
  }
  
  
}
