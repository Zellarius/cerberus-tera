import processing.core.*;

public class MapGenerator ////SINGLETON
{
  PImage map;
  int ID; //For the filename
  private static MapGenerator instance;
  private static PApplet p;
  int [] hot; //map colors options
  float n0,n1,n;
  int k;
  int rand;
  float d0,d1;
  /*
 // If you want the reverse map feeling
  color[] hot = {
  color(255, 129, 63), 
  color(231, 101, 47), 
  color(198, 87, 41), 
  color(161, 168, 25), 
  color(127, 56, 27), 
  color(92, 39, 20), 
  color(58, 26, 12), 
  color(25, 11, 5), 
  color(120, 46, 192), 
  color(52, 28, 148), 
  color(0, 28, 102), 
  color(0, 50, 128), 
  color(21, 119,  185), 
  color(46, 151, 208), 
  color(68, 179, 240), 
  color(107, 199, 255)
}; */
 
 
  
  private MapGenerator()
  {
  }
  
  public static MapGenerator getInstance(PApplet papp)
  {
    if(instance==null)
    {
      instance= new MapGenerator();
      p=papp;
    }
    return instance;
  }
  
  void Generate()  //Generator the map
  {    
          hot = new int[] {
        p.color(0, 126, 192), 
        p.color(24, 154, 208), 
        p.color(58, 168, 214), 
        p.color(94, 186, 220), 
        p.color(128, 199, 228), 
        p.color(163, 216, 235), 
        p.color(197, 229, 243), 
        p.color(232, 244, 250), 
        p.color(135, 209, 63), 
        p.color(203, 227, 107), 
        p.color(255, 227, 153), 
        p.color(255, 205, 127), 
        p.color(234, 136, 70), 
        p.color(209, 104, 47), 
        p.color(187, 76, 15), 
        p.color(148, 56, 0)
        /*  color(255, 129, 63), 
  p.color(231, 101, 47), 
  p.color(198, 87, 41), 
  p.color(161, 168, 25), 
  p.color(127, 56, 27), 
  p.color(92, 39, 20), 
  p.color(58, 26, 12), 
  p.color(25, 11, 5), 
  p.color(120, 46, 192), 
  p.color(52, 28, 148), 
  p.color(0, 28, 102), 
  p.color(0, 50, 128), 
  p.color(21, 119,  185), 
  p.color(46, 151, 208), 
  p.color(68, 179, 240), 
  p.color(107, 199, 255)*/
      };
      
        ID=(int)(p.random(10000000));
        rand=(int)(p.random(0,100));
        p.noiseSeed(rand);
        p.loadPixels(); 
        d0 = p.random(100, 200);   
        d1 = p.random(25, 75); 
        
      d0 = p.random(100, 200);   
      d1 = p.random(25, 75); 
      for (int j = 0; j < p.height; j++) {
        for (int i = 0; i < p.width; i++) { 
          n0 = p.noise(i/d0, j/d0, 0); 
          n1 = p.noise(i/d1, j/d1, 10); 
          n = (float)(1 - ((n0*0.75) + (n1*0.25))); 
          k = (int)(n*hot.length); 
          p.pixels[j*p.width + i] = hot[k];
        }
      }
    }

  void Save() //Save the map as a file
  { 
    p.updatePixels();
    p.save("maps/MapID" + ID + ".png");
  }
  
  void Load() //Load the file (to reduce lag)
  {
    map = p.loadImage("maps/MapID" + ID + ".png");    
  }
  
  void Show() //Show map image
  {
    p.image(map, 0, 0);
  }
}
