Galaxy Acturus;

void setup() {
  Acturus = new Galaxy(this); // Galaxy contains everything
  size(1600, 900);
  
}
void draw() {
  background(50);
  Acturus.RunState();
}

void keyPressed()
{
  if(key=='r')
  {
    setup();
  }
  if(key=='a')
  {
    if(Acturus.spaceBoids.autoSpawn==false)
    {
      Acturus.spaceBoids.autoSpawn=true;
    }
    else
    {
      Acturus.spaceBoids.autoSpawn=false;
    }
  }
}
