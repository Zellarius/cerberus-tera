  class Flock {
  Factory factory;
  ArrayList<BoidGen> boids;
  BoidGen dead;
  boolean oneDown=false, eating=false, del=false, autoSpawn=true;
  PVector spawnpointRed;
  PVector spawnpointBlue;
  int nbBlue, nbRed, nbNeutral;
  int minBlue, minNeutral;
  Laser l;
  int currenttime, wait, lasttime;
  int tmp;
  BoidBlue blue;
  BoidRed red;
  BoidNeutral neutral;

  

  
  Flock(int b, int r, int n) {
    factory= new Factory(); //Boid Factory
    //Randomized initial spawnpoint
      spawnpointRed = new PVector();
      spawnpointRed.x= (width/6)*int(random(0,6));
      spawnpointRed.y= (height/6)*int(random(0,6));
      spawnpointBlue = new PVector();
      spawnpointBlue.x= (width/6)*int(random(0,6));
      spawnpointBlue.y= (height/6)*int(random(0,6));
      nbBlue=b;
      nbRed=r;
      nbNeutral=n;
      minBlue=10;
      minNeutral=10;
      currenttime = millis();
      wait=2000;
      lasttime=0;
      
    boids = new ArrayList<BoidGen>(); // Initialize the ArrayList
    
    for(int a=0; a<b; a++) // SPAWN ALEATOIRE INITIAL
    {
      boids.add(factory.createBoid(spawnpointRed.x, spawnpointRed.y, 'r'));
    }
    for(int a=0; a<r; a++)
    {
      boids.add(factory.createBoid(spawnpointBlue.x, spawnpointBlue.y, 'b'));
    }
    for(int a=0; a<n; a++)
    {
      boids.add(factory.createBoid(spawnpointRed.x, spawnpointRed.y, 'n'));
    }
  }
  
  void textShow() //AFFICHAGE DE TOUT LES TEXTES
  {
    if(autoSpawn==true)
    {
      textSize(32);
      fill(255, 255, 255);
      text("Autospawn: On", 45, 30);   
    }
    else
    {
      textSize(32);
      fill(255, 100, 100);
      text("Autospawn: Off", 45, 30);   
    }
    textSize(32);
    fill(0, 0, 255);
    text("Blue: "+nbBlue, 0, height-15);
    textSize(32);
    fill(255, 0, 0);
    text("Red:"+nbRed, width/2, height-15); 
    textSize(32);
    fill(255, 255, 255);
    text("Neutral:"+nbNeutral, width-300, height-15); 
  }


  void run() { //Function managing the red killing blue and blue killing greys
    for (BoidGen b : boids) {
      b.run(boids); 
      
      if(b.squad=='b')
      {
        if(b.state==1)
        {blue=(BoidBlue)(b);
          eating=blue.blueEat(boids);
          
          if(eating)
          {           
            dead=blue.target;
          }
        }
      }
    }
    if(eating)
    {     
      eating=false;
      boids.remove(dead);
    }
    for(BoidGen b : boids)
    {
      laserthing(b,boids);
    }
    
    if(oneDown==true)
    {  
       boids.remove(dead);
       oneDown=false;
    }
    
    lifeGame();   
    textShow();
  }
  
  void lifeGame() //RESPAWN DS BOIDS POUR LE JEU DE LA VIE
  {   
    nbBlue=0;
    nbRed=0;
    nbNeutral=0;
    for (BoidGen b : boids) {
      if(b.squad=='b')
      {
        nbBlue++;
      }
      else if(b.squad=='r')
      {
        nbRed++;
      }
      else if(b.squad=='n')
      {
        nbNeutral++;
      }
    }
      //Auto Spawn Manager
     currenttime = millis();
    if((currenttime-lasttime)>=wait)
    {
      lasttime=currenttime;
      if(autoSpawn==true)
      {
        if(nbBlue<=minBlue)
        {          
          for(int i=0;i<5;i++)
          {
            boids.add(factory.createBoid(width/4, height/2, 'b'));
          }
        }
        if(nbNeutral<=minNeutral)
        {              
          for(int i=0;i<5;i++)
          {
            boids.add(factory.createBoid((width/4)*3, height/2, 'n'));
          }
        }
      }
    }
  }
  
  
void laserthing(BoidGen b, ArrayList <BoidGen> boids) //GESTION DE COLISION DES LASERS
{
  if(b.squad=='r')
  red=(BoidRed)(b);
  if(red.max_laser>0)
  {
    for (Laser j : red.lasers) 
    {
      //Check edge of map
        j.update(); 
        if((j.position.x<2)||(j.position.x>width-2)||(j.position.y<2)||(j.position.y>height+2))
        {
           l=j;
           del=true;
        }
        //Check other boids
        for(BoidGen other : boids)
        {
          if(other.squad=='b')
          {
            if(j.checkCollision(other.position))
             {
               dead=other;
               oneDown=true;
               l=j;
               del=true;
             }
          }
        }        
     }    
  }
    
  if(del==true)
  {
    red.max_laser--;
    red.lasers.remove(l);
    del=false;
  }
 }

void command(char c, boolean ar) // COMMANDES POUR RESEAUTIQUE
{  
  //ar= add or remove. true=add. false=remove
  if(c=='b')
  {
    if(ar==true)
    {
        boids.add(factory.createBoid(width/4, height/2, 'b'));
    }
    else
    {
      if(nbBlue>0)
      {
        for (BoidGen b : boids) {
          if(b.squad=='b')
          {
            dead=b;
            oneDown=true;
          }
        }
      }
    }
  }
  else if(c=='r')
  {
    if(ar==true)
    {
       boids.add(factory.createBoid(width/3, height/2, 'r'));
    }
    else
    {
      if(nbRed>0)
      {
        for (BoidGen b : boids) {
          if(b.squad=='r')
          {
            dead=b;
            oneDown=true;
          }
        }
      }
    }
  }
  else if(c=='n')
  {
    if(ar==true)
    {
       boids.add(factory.createBoid(width/3, height/2, 'n'));
    }
    else
    {
      if(nbNeutral>0)
      {
        for (BoidGen b : boids) {
          if(b.squad=='n')
          {
            dead=b;
            oneDown=true;
          }
        }
      }
    }
  }
}
}
