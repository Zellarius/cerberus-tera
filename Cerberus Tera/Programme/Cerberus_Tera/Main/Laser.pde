class Laser {
 PVector position;
 PVector velocity;
  
 
 Laser(float r,PVector plo)
 {
   position=new PVector();
   velocity = new PVector();
   position.x= plo.x;
   position.y=plo.y;
   float angle = r - PI/2;
   velocity= PVector.fromAngle(angle);
   velocity.mult(6);
 }
  
  void display()
  {
    fill(255,255,0);
    rect(position.x,position.y,5,5);
  }
  
    void update() 
  { 
    display();
    position.add(velocity);
  }
  
  
  boolean checkCollision( PVector location) //Check colisions with sent location
  {
    if((position.x <= location.x+10)&&(position.x >= location.x-10)&&(position.y <= location.y+10)&&(position.y >= location.y-10))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
