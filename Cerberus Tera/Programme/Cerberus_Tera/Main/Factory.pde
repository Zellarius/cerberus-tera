class Factory {
  
  Factory()
  {
  }
  
   public BoidGen createBoid(float x, float y, char team){    
      if(team=='r'){
         return new BoidRed(x,y);
      } else if(team=='b'){
         return new BoidBlue(x,y);
      } else if(team=='n'){
         return new BoidNeutral(x,y);
      }
      return null;
   }
}
