public class Tera
{

  PVector size;
  MapGenerator mapgen;
  PApplet p;
  public Tera(PApplet papp)
  {
    p=papp;
    mapgen= MapGenerator.getInstance(p);
  }
  
   void initialisation()
  {
    mapgen.Generate();
    mapgen.Save();
    mapgen.Load();
  }
   void display()
  {
      mapgen.Show();
  }
}
