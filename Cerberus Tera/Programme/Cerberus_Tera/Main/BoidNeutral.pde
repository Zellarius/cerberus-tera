// The Neutral Boid class

class BoidNeutral extends BoidGen {

  BoidGen target;
  PVector faceEnemy;

    BoidNeutral(float x, float y) {
    squad='n';
    acceleration = new PVector(0, 0);
    faceEnemy=new PVector();
    float angle = random(TWO_PI);
    velocity = new PVector(cos(angle), sin(angle));
    position = new PVector(x, y);
    r = 5.0;
    maxspeed = 2;
    maxforce = 0.03;
  }

  // We accumulate a new acceleration each time based on three rules
  @Override void flock(ArrayList<BoidGen> boids) { //Override due to All boid flocking behavior being different
    
      PVector sep = separate(boids);   // Separation
      PVector ali = align(boids);      // Alignment
      PVector coh = cohesion(boids);   // Cohesion
      // Arbitrarily weight these forces
      sep.mult(1.5);
      ali.mult(1.0);
      coh.mult(1.0);
      // Add the force vectors to acceleration
      applyForce(sep);
      applyForce(ali);
      applyForce(coh);
  }

  @Override void render() {
       theta = velocity.heading2D() + radians(90);
    fill(100,100,100);
    
    stroke(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(theta);
    beginShape(TRIANGLES);
    vertex(0, -r*2);
    vertex(-r, r*2);
    vertex(r, r*2);
    endShape();
    popMatrix();
  }

  // Separation
  // Method checks for nearby boids and steers away
  @Override PVector separate (ArrayList<BoidGen> boids) {//Flocking behavior, separation function
    float desiredseparation = 25.0f;
    PVector steer = new PVector(0, 0, 0);
    int count = 0;
    // For every boid in the system, check if it's too close
    for (BoidGen other : boids) {
      float d = PVector.dist(position, other.position);
      // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
      if((other.squad==squad)||((squad=='n')&&(other.squad=='r')))
      {
        if ((d > 0) && (d < desiredseparation)) {
          // Calculate vector pointing away from neighbor
          PVector diff = PVector.sub(position, other.position);
          diff.normalize();
          diff.div(d);        // Weight by distance
          steer.add(diff);
          count++;            // Keep track of how many
          }
      }
    }
    if (count > 0) {
      steer.div((float)count);
    }

    // As long as the vector is greater than 0
    if (steer.mag() > 0) {
      
      steer.normalize();
      steer.mult(maxspeed);
      steer.sub(velocity);
      steer.limit(maxforce);
    }
    return steer;
  }

  // Alignment
  @Override PVector align (ArrayList<BoidGen> boids) {//Flocking behavior, alignent function
    float neighbordist = 50;
    PVector sum = new PVector(0, 0);
    int count = 0;
    for (BoidGen other : boids) {
      if((other.squad==squad)||((squad=='n')&&(other.squad=='r')))
      {
        float d = PVector.dist(position, other.position);
        if ((d > 0) && (d < neighbordist)) {
          sum.add(other.velocity);
          count++;
        }
      }
    }
    if (count > 0) {
      sum.div((float)count);

      sum.normalize();
      sum.mult(maxspeed);
      PVector steer = PVector.sub(sum, velocity);
      steer.limit(maxforce);
      return steer;
    } 
    else {
      return new PVector(0, 0);
    }
  }

  // Cohesion
  @Override PVector cohesion (ArrayList<BoidGen> boids) {//Flocking behavior, cohesion function
    float neighbordist = 50;
    PVector sum = new PVector(0, 0);  
    int count = 0;
    for (BoidGen other : boids) {
       if((other.squad==squad)||((squad=='n')&&(other.squad=='r')))
      {
        if((squad=='n')&&(other.squad=='r'))
        {
          float d = PVector.dist(position, other.position);
          if ((d > 0) && (d < neighbordist*2)) {
            sum.add(other.position); // Add position
            count++;
          }
        }
        else
        {
          float d = PVector.dist(position, other.position);
          if ((d > 0) && (d < neighbordist)) {
            sum.add(other.position); // Add position
            count++;
          }
        }
      }
    }
    if (count > 0) {
      sum.div(count);
      return seek(sum);  // Steer towards the position
    } 
    else {
      return new PVector(0, 0);
    }
  }
}
