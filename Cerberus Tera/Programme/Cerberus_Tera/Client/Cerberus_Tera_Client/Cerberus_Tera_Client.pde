import processing.net.*;

BarreBoutons bb;

ArrayList <Bouton> clickedButton;

Button Add;
Button Reset;
Button Delete;
String team;
Button Selected;

int bpp = 3;

int currentColor = color (255);

void setup () {
  size (600, 600);
  
  initBB();
  initSelections();
  //initButtons();
  
}

void update() {
  manageMouse();
  Add.update();
  Reset.update();
  Delete.update();
  
}


void initBB() {//Initialisation of button bar
  bb = new BarreBoutons(3, 0, 10);
  
  bb.setBackgroundColorAt(0, color(150),"Neutral");
  bb.setEnabledBorderColorAt(0, color(255));
  
  
  bb.setBackgroundColorAt(1, color(255, 0, 0),"Red");
  bb.setEnabledBorderColorAt(1, color(255));
  
  bb.setBackgroundColorAt(2, color(0, 0, 255),"Blue");
  bb.setEnabledBorderColorAt(2, color(255));
  
  bb.horizontal = false;
  bb.exclusif = true;
  bb.mettreAJour();
}

void initSelections()
{
  Add=new Button((width/6),height/2,"Add");
  Reset=new Button((width/6)*2,height/2,"Reset");
  Delete= new Button((width/6)*3,height/2,"Delete");
}


void display() {
  bb.display();
   Add.show();
   Reset.show();
   Delete.show();
}


void manageMouse() {
  if (mousePressed) {
    int mX = mouseX;
    int mY = mouseY;
    
    if (mouseButton == LEFT) {
      bb.updateClick();
      if(Add.overRect())
      { Selected=Add;
      Add.selected=true;
      Delete.selected=false;
      Reset.selected=false;
      }
      else if(Delete.overRect())
      {Selected=Delete;
      Add.selected=false;
      Delete.selected=true;
      Reset.selected=false;
      }
      else if(Reset.overRect())
      {Selected=Reset;
      Add.selected=false;
      Delete.selected=false;
      Reset.selected=true;
      }
      
      manageButtonBar();

      }    
      
  }  
}

void manageButtonBar() {
  clickedButton = bb.getActiveButtons();
  
  if (clickedButton.size() > 0) {
    currentColor = clickedButton.get(0).couleurFond;
    team=clickedButton.get(0).team;
  }
}

void draw () {
  update();
  display();
}

void keyPressed () {
  if (key == 's') {
    println ("sending...");
    sendData();
  }
}

void sendData() {
  Client c;
  String ipAddress = "127.0.0.1";
  int port = 32999;


  c = new Client (this, ipAddress, port);
  
  JSONObject json = new JSONObject();
  
  json.setString ("Action", Selected.action);
  json.setString("Team", team );

  c.write(json.toString() + "~");
  //c.stop();
  c = null;
}
