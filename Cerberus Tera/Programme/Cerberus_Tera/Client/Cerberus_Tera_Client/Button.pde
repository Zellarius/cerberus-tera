class Button
{
  int rectX, rectY; 
  color buttonColor;
  boolean rectOver = false;
  color overColor;
  String action;
  int rectW;
  int rectH;
  color selectedColor;
  boolean selected=false;
  
  Button(int posx, int posy, String Action)
  {
    buttonColor=color(200);
    overColor=color(0,100,100);
    selectedColor=color(0,200,0);
    rectX=posx;
    rectY=posy;
    this.action=Action;
    rectW=width/6;
    rectH=width/6;
  }
  
  
  void show()
  {
    if(selected)
    {fill(selectedColor);}
    else if(rectOver==true)
    {fill(overColor);}
    else
    {fill(buttonColor);}
    
    stroke(0);
    rect(rectX, rectY, rectW, rectH);
    
    if(action=="Add")
    {
      textSize(32);
      fill(0);
      text("Add", (rectX), rectY); 
    }
    else if(action=="Delete")
    {
      textSize(32);
      fill(0);
      text("Delete", (rectX), rectY); 
    }
    else if(action=="Reset")
    {
      textSize(32);
      fill(0);
      text("Reset", (rectX), rectY); 
    }
  }
  
  void update()
  {
    if(overRect())
    {
      rectOver=true;
    }
    else
    {
      rectOver=false;
    }
  }
  
  boolean overRect()  {
  if (mouseX >= rectX && mouseX <= rectX+rectW && 
      mouseY >= rectY && mouseY <= rectY+rectH) {
    return true;
  } else {
    return false;
  }
}
}
